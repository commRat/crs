#!/usr/bin/python

import sys
import time
import arrow


def compare_time(current_month, last_month):
    return True if current_month != last_month else False


def scrap_latest_movies():
    # TODO
    return None


def get_time():
    # TODO get It from db
    return "2022 1"


def main():
    last_month = get_time()
    t = time.time()
    current_month = arrow.get(t).to("local").format("YYYY M")
    get_new_data = compare_time(current_month, last_month)
    if get_new_data:
        scrap_latest_movies()
    return 0


if __name__ == '__main__':
	sys.exit(main())