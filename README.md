## Cinema Reservation System - CRS

### Description

CRS will store the latest movies (scraped from website). There will be 4 random movies in 2 cinemas every day. User will be able to register exact seats, if they will be empty. 

### Development

I will probably use my randombase library - for generating seat occupancy. Flask with SQLite3 as backend & most likely Mithril or React for frontend.

Stages:
- pure python functions
- http API (testing with curl)
- http gui
